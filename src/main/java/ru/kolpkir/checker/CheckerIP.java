package ru.kolpkir.checker;

import java.util.regex.*;

public class CheckerIP implements IChecker {
    private final String regex = "^(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)$";
    private Pattern checkRegex;

    public CheckerIP() {
        checkRegex = Pattern.compile(regex);
    }

    public boolean Check(String checkedString) {
        Matcher regexMatcher = checkRegex.matcher(checkedString);
        return regexMatcher.matches();
    }
}
