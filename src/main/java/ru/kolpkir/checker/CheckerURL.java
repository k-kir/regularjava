package ru.kolpkir.checker;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckerURL implements IChecker {
    private final String regex = "^(https?://)?((?!-)([\\dA-Za-z-])*([\\dA-Za-z])\\.)*((?!-)([\\dA-Za-z-])+([\\dA-Za-z])\\.)((?!-)([\\dA-Za-z-])+([\\dA-Za-z]))(:\\d{1,5})?(/[\\w.-]+)*/?(\\?([\\w-]+=[\\w-]+&)*([\\w-]+=[\\w-]+))?(#[\\w-]+)?$";
    private Pattern checkRegex;

    public CheckerURL() {
        checkRegex = Pattern.compile(regex);
    }

    public boolean Check(String checkedString) {
        Matcher regexMatcher = checkRegex.matcher(checkedString);
        return regexMatcher.matches();
    }
}
