package ru.kolpkir.checker;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckerPassword implements IChecker {
    private final String regex = "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])[\\w-]{8,}$";
    private Pattern checkRegex;

    public CheckerPassword() {
        checkRegex = Pattern.compile(regex);
    }

    public boolean Check(String checkedString) {
        Matcher regexMatcher = checkRegex.matcher(checkedString);
        return regexMatcher.matches();
    }
}
