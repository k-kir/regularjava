package ru.kolpkir.checker;

import java.util.regex.*;

public class CheckerGUID implements IChecker {
    private final String regex = "^([0-9a-fA-F]{8}-([0-9a-fA-F]{4}-){3}[0-9a-fA-F]{12}|\\{[0-9a-fA-F]{8}-([0-9a-fA-F]{4}-){3}[0-9a-fA-F]{12}})$";
    private Pattern checkRegex;

    public CheckerGUID() {
        checkRegex = Pattern.compile(regex);
    }

    public boolean Check(String checkedString) {
        Matcher regexMatcher = checkRegex.matcher(checkedString);
        return regexMatcher.matches();
    }
}
