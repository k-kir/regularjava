import org.junit.jupiter.api.*;
import ru.kolpkir.checker.CheckerPassword;

public class TestPassword {
    private static CheckerPassword checker;

    @BeforeAll
    public static void init() {
        checker = new CheckerPassword();
    }

    @AfterAll
    public static void tearDown() {
        checker = null;
    }

    @Test
    void testTrue0() {
        Assertions.assertTrue(checker.Check("C00l_Pass"));
    }

    @Test
    void testTrue1() {
        Assertions.assertTrue(checker.Check("SupperPas1"));
    }

    @Test
    void testTrue2() {
        Assertions.assertTrue(checker.Check("Password1"));
    }

    @Test
    void testTrue3() {
        Assertions.assertTrue(checker.Check("password1D"));
    }

    @Test
    void testTrue4() {
        Assertions.assertTrue(checker.Check("aaswordE5"));
    }

    @Test
    void testTrue5() {
        Assertions.assertTrue(checker.Check("4PassWord"));
    }

    @Test
    void testTrue6() {
        Assertions.assertTrue(checker.Check("P4SSWoRD"));
    }

    @Test
    void testTrue7() {
        Assertions.assertTrue(checker.Check("aP4SS3WORD"));
    }

    @Test
    void testTrue8() {
        Assertions.assertTrue(checker.Check("PaSS1010WoRD"));
    }

    @Test
    void testTrue9() {
        Assertions.assertTrue(checker.Check("pASSW0RD"));
    }

    @Test
    void testFalse0() {
        Assertions.assertFalse(checker.Check("pASSWORD"));
    }

    @Test
    void testFalse1() {
        Assertions.assertFalse(checker.Check("Cool_pass"));
    }

    @Test
    void testFalse2() {
        Assertions.assertFalse(checker.Check("C00l"));
    }

    @Test
    void testFalse3() {
        Assertions.assertFalse(checker.Check("cool_p4ss"));
    }

    @Test
    void testFalse4() {
        Assertions.assertFalse(checker.Check("coolpAss"));
    }

    @Test
    void testFalse5() {
        Assertions.assertFalse(checker.Check("COOLP4SS"));
    }

    @Test
    void testFalse6() {
        Assertions.assertFalse(checker.Check("password"));
    }

    @Test
    void testFalse7() {
        Assertions.assertFalse(checker.Check("4password"));
    }

    @Test
    void testFalse8() {
        Assertions.assertFalse(checker.Check("password3"));
    }

    @Test
    void testFalse9() {
        Assertions.assertFalse(checker.Check("PASSWORD2"));
    }
}
