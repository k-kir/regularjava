import org.junit.jupiter.api.*;
import ru.kolpkir.checker.CheckerURL;

public class TestURL {
    private static CheckerURL checker;

    @BeforeAll
    public static void init() {
        checker = new CheckerURL();
    }

    @AfterAll
    public static void tearDown() {
        checker = null;
    }

    @Test
    void testTrue0() {
        Assertions.assertTrue(checker.Check("vyatsu.ru"));
    }

    @Test
    void testTrue1() {
        Assertions.assertTrue(checker.Check("e.vyatsu.ru"));
    }

    @Test
    void testTrue2() {
        Assertions.assertTrue(checker.Check("http://e.vyatsu.ru"));
    }

    @Test
    void testTrue3() {
        Assertions.assertTrue(checker.Check("https://e.vyatsu.ru/"));
    }

    @Test
    void testTrue4() {
        Assertions.assertTrue(checker.Check("https://m.vsu.kolpkir.ru/?abc=def&hhs=3#qwe"));
    }

    @Test
    void testTrue5() {
        Assertions.assertTrue(checker.Check("https://m.vsu.kolpkir.ru/?abc=def&hhs=3"));
    }

    @Test
    void testTrue6() {
        Assertions.assertTrue(checker.Check("https://m.vsu.kolpkir.ru/?abc=def"));
    }

    @Test
    void testTrue7() {
        Assertions.assertTrue(checker.Check("https://m.vsu.kolpkir.ru/#qwe"));
    }

    @Test
    void testTrue8() {
        Assertions.assertTrue(checker.Check("https://m.vsu.kolpkir.ru/sub/dir/?abc=def&hhs=3#qwe"));
    }

    @Test
    void testTrue9() {
        Assertions.assertTrue(checker.Check("https://m.v-su.kolpkir.ru/sub/file?abc=def&hhs=3#qwe"));
    }

    @Test
    void testTrue10() {
        Assertions.assertTrue(checker.Check("https://m.vsu.kolpkir.ru:123/sub/file?abc=def&hhs=3#qwe"));
    }

    @Test
    void testTrue11() {
        Assertions.assertTrue(checker.Check("xn--b1alfrj.xn--p1ai"));
    }

    @Test
    void testFalse0() {
        Assertions.assertFalse(checker.Check("httq://m.vsu.kolpkir.ru/sub/file?abc=def&hhs=3#qwe"));
    }

    @Test
    void testFalse1() {
        Assertions.assertFalse(checker.Check("https://m.vsu.kolpkir.ru//"));
    }

    @Test
    void testFalse2() {
        Assertions.assertFalse(checker.Check("https://m.vsu.kolpkir.ru/sub//file?abc=def&hhs=3#qwe"));
    }

    @Test
    void testFalse3() {
        Assertions.assertFalse(checker.Check("https://m.vsu.kolpkir.ru:123456/sub/file?abc=def&hhs=3#qwe"));
    }

    @Test
    void testFalse4() {
        Assertions.assertFalse(checker.Check("https://m.vsu.kolpkir.ru/sub/file?abcdef&hhs=3#qwe"));
    }

    @Test
    void testFalse5() {
        Assertions.assertFalse(checker.Check("https://m.vsu.kolpkir.ru/sub/file?abc=defhhs=3#qwe"));
    }

    @Test
    void testFalse6() {
        Assertions.assertFalse(checker.Check("https://m.vsu.kolpkir.ru/sub/file?abc=def&hhs3#qwe"));
    }

    @Test
    void testFalse7() {
        Assertions.assertFalse(checker.Check("https://m.vsu.kolpkir.ru/sub/fileabc=def&hhs=3#qwe"));
    }

    @Test
    void testFalse8() {
        Assertions.assertFalse(checker.Check("https://m.ru/sub/file?abc=def&hhs=3#qwe"));
    }

    @Test
    void testFalse9() {
        Assertions.assertFalse(checker.Check("test"));
    }

    @Test
    void testFalse10() {
        Assertions.assertFalse(checker.Check("https://m.vsu.kolp kir.ru/sub/file?abc=def&hhs=3#qwe"));
    }

    @Test
    void testFalse11() {
        Assertions.assertFalse(checker.Check("https://m.vsu.kolp_kir.ru/sub/file?abc=def&hhs=3#qwe"));
    }
}
