import org.junit.jupiter.api.*;
import ru.kolpkir.checker.CheckerGUID;

public class TestGUID {
    private static CheckerGUID checker;

    @BeforeAll
    public static void init() {
        checker = new CheckerGUID();
    }

    @AfterAll
    public static void tearDown() {
        checker = null;
    }

    @Test
    void testTrue0() {
        Assertions.assertTrue(checker.Check("22345200-abe8-4f60-90c8-0d43c5f6c0f6"));
    }

    @Test
    void testTrue1() {
        Assertions.assertTrue(checker.Check("{22345200-abe8-4f60-90c8-0d43c5f6c0f6}"));
    }

    @Test
    void testTrue2() {
        Assertions.assertTrue(checker.Check("8cac2ab1-558a-4727-982f-eb083b6ae516"));
    }

    @Test
    void testTrue3() {
        Assertions.assertTrue(checker.Check("10e5526b-1a3c-4291-a84b-f616d786adba"));
    }

    @Test
    void testTrue4() {
        Assertions.assertTrue(checker.Check("{115958d4-7e11-409b-86e4-463432c8e931}"));
    }

    @Test
    void testTrue5() {
        Assertions.assertTrue(checker.Check("{e3dbe6d2-28a2-46d3-8029-b571660e55e2}"));
    }

    @Test
    void testTrue6() {
        Assertions.assertTrue(checker.Check("{23A5D2F5-53F4-4086-B06D-A7E59D2E1E67}"));
    }

    @Test
    void testTrue7() {
        Assertions.assertTrue(checker.Check("{221DF117-376D-4FC2-BFF6-AE74172BBC8A}"));
    }

    @Test
    void testTrue8() {
        Assertions.assertTrue(checker.Check("F674A329-C207-49E4-A2A7-5B1DA19197B7"));
    }

    @Test
    void testTrue9() {
        Assertions.assertTrue(checker.Check("70D920CD-82FB-4B7D-8861-1B45CD1002B4"));
    }

    @Test
    void testFalse0() {
        Assertions.assertFalse(checker.Check("70D920CD82FB-4B7D-8861-1B45CD1002B4"));
    }

    @Test
    void testFalse1() {
        Assertions.assertFalse(checker.Check("70D920CD-82FB4B7D-8861-1B45CD1002B4"));
    }

    @Test
    void testFalse2() {
        Assertions.assertFalse(checker.Check("70D920CD-82FB-4B7D8861-1B45CD1002B4"));
    }

    @Test
    void testFalse3() {
        Assertions.assertFalse(checker.Check("70D920CD-82FB-4B7D-88611B45CD1002B4"));
    }

    @Test
    void testFalse4() {
        Assertions.assertFalse(checker.Check("{22345200-abe8-4f60-90c8-0d43c5f6c0f6"));
    }

    @Test
    void testFalse5() {
        Assertions.assertFalse(checker.Check("22345200-abe8-4f60-90c8-0d43c5f6c0f6}"));
    }

    @Test
    void testFalse6() {
        Assertions.assertFalse(checker.Check("{22345200-abx8-4f60-90c8-0d43c5f6c0f6}"));
    }

    @Test
    void testFalse7() {
        Assertions.assertFalse(checker.Check("{2234-5200-abe8-4f60-90c8-0d43c5f6c0f6}"));
    }

    @Test
    void testFalse8() {
        Assertions.assertFalse(checker.Check("2234-5200-abe8-4f60-90c8-0d43-c5f6-c0f6"));
    }

    @Test
    void testFalse9() {
        Assertions.assertFalse(checker.Check("{2234-5200-abe8-4f60-90c8-0d43c5f6-c0f6}"));
    }
}
