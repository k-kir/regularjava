import org.junit.jupiter.api.*;
import ru.kolpkir.checker.CheckerIP;

public class TestIP {
    private static CheckerIP checker;

    @BeforeAll
    public static void init() {
        checker = new CheckerIP();
    }

    @AfterAll
    public static void tearDown() {
        checker = null;
    }

    @Test
    void testTrue0() {
        Assertions.assertTrue(checker.Check("192.168.0.1"));
    }

    @Test
    void testTrue1() {
        Assertions.assertTrue(checker.Check("127.0.0.1"));
    }

    @Test
    void testTrue2() {
        Assertions.assertTrue(checker.Check("217.9.149.170"));
    }

    @Test
    void testTrue3() {
        Assertions.assertTrue(checker.Check("8.8.8.8"));
    }

    @Test
    void testTrue4() {
        Assertions.assertTrue(checker.Check("74.125.131.94"));
    }

    @Test
    void testTrue5() {
        Assertions.assertTrue(checker.Check("77.88.55.55"));
    }

    @Test
    void testTrue6() {
        Assertions.assertTrue(checker.Check("87.226.192.100"));
    }

    @Test
    void testTrue7() {
        Assertions.assertTrue(checker.Check("0.0.0.0"));
    }

    @Test
    void testTrue8() {
        Assertions.assertTrue(checker.Check("104.16.132.229"));
    }

    @Test
    void testTrue9() {
        Assertions.assertTrue(checker.Check("255.255.255.255"));
    }

    @Test
    void testFalse0() {
        Assertions.assertFalse(checker.Check("255.256.255.255"));
    }

    @Test
    void testFalse1() {
        Assertions.assertFalse(checker.Check("1420.11.11.11"));
    }

    @Test
    void testFalse2() {
        Assertions.assertFalse(checker.Check("01.01.01.01"));
    }

    @Test
    void testFalse3() {
        Assertions.assertFalse(checker.Check("a.b.c.d"));
    }

    @Test
    void testFalse4() {
        Assertions.assertFalse(checker.Check("123.321.250.111"));
    }

    @Test
    void testFalse5() {
        Assertions.assertFalse(checker.Check("1.2.3.4.5"));
    }

    @Test
    void testFalse6() {
        Assertions.assertFalse(checker.Check("1:2:3:4"));
    }

    @Test
    void testFalse7() {
        Assertions.assertFalse(checker.Check("99.88.77.66."));
    }

    @Test
    void testFalse8() {
        Assertions.assertFalse(checker.Check(".55.44.33.22"));
    }

    @Test
    void testFalse9() {
        Assertions.assertFalse(checker.Check("87.88.89.1h2"));
    }
}
